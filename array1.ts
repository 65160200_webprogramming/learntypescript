const names: string[] = []
names.push("Dylan")
names.push("Sittipong")

console.log(names[0])
console.log(names[1])
console.log(names.length)

for (let index = 0; index < names.length; index++) {
    const element = names[index];
    console.log(element)
}

for (let ind in names) {
    console.log(names[ind])
}

names.forEach(function(name) {
    console.log(name)
})
