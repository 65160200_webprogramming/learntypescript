let d: unknown = "Hello"
let num: string =  "1"
console.log((d as string).length)
console.log((<string>d).length)
console.log(((num as unknown) as number).toFixed(2))